﻿using SharedComponents.BaseImplementations;
using SharedComponents.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedComponents.Models
{
    public class Stat : BaseNotifyPropertyChanged, IGameObject
    {

        #region Properties
        private bool MinValueLimited { get; set; }
        private bool MaxValueLimited { get; set; }
        
        private int _Id;

        public int GameId
        {
            get { return _Id; }
            set
            {
                _Id = value;
                NotifyPropertyChanged();
            }
        }

        private string _Name;

        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        private string _Description;

        public string Description
        {
            get { return _Description; }
            set
            {
                _Description = value;
                NotifyPropertyChanged();
            }
        }

        private int _Value;

        public int Value
        {
            get { return _Value; }
            set 
            { 
                _Value = value;
                if (!MinValueLimited  
                    && _Value < _MinValue)
                {
                    _Value = _MinValue;
                }
                if (!MaxValueLimited
                    && _Value > _MaxValue)
                {
                    _Value = _MaxValue;
                }
                NotifyPropertyChanged();
            }
        }

        private int _MinValue;

        public int MinValue
        {
            get { return _MinValue; }
            set
            {
                _MinValue = value;
                NotifyPropertyChanged();
            }
        }

        private int _MaxValue;

        public int MaxValue
        {
            get { return _MaxValue; }
            set
            {
                _MaxValue = value;
                NotifyPropertyChanged();
            }
        }
        #endregion


        #region Constructor
        /// <summary>
        /// Non Mix Max constructor
        /// </summary>
        /// <param name="Name"></param>
        public Stat(string Name)
        {
            this.Name = Name;
            this.MaxValueLimited = false;
            this.MinValueLimited = false;
        }
        
        /// <summary>
        /// Full package constructor
        /// </summary>
        /// <param name="Name"></param>
        /// <param name="MinValue"></param>
        /// <param name="MaxValue"></param>
        /// <param name="MaxValueLimited"></param>
        /// <param name="MinValueLimited"></param>
        public Stat(string Name, int MinValue, int MaxValue, bool MaxValueLimited, bool MinValueLimited)
        {
            this.Name = Name;
            this.MaxValue = MaxValue;
            this.MinValue = MinValue;
            this.MaxValueLimited = MaxValueLimited;
            this.MinValueLimited = MinValueLimited;
        }
        #endregion
    }
}
