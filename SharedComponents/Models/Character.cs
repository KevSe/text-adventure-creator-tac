﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedComponents.BaseImplementations;
using SharedComponents.Interfaces;

namespace SharedComponents.Models
{
    public class Character : BaseNotifyPropertyChanged
    {
        #region Properties
        private int _Id;

        public int Id
        {
            get { return _Id; }
            set
            {
                _Id = value;
                NotifyPropertyChanged();
            }
        }

        private string _Name;

        public string Name
        {
            get { return _Name; }
            set
            {
                _Name = value;
                NotifyPropertyChanged();
            }
        }

        private string _Description;

        public string Description
        {
            get { return _Description; }
            set
            {
                _Description = value;
                NotifyPropertyChanged();
            }
        }


        private ObservableCollection<GameItem> _InventoryItems;

        public virtual ObservableCollection<GameItem> InventoryItems
        {
            get { return _InventoryItems; }
            set { _InventoryItems = value; }
        }

        // TODO: Implement Actions

        public virtual Game Game { get; set; }
        #endregion
    }
}
