﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedComponents.BaseImplementations;
using SharedComponents.Interfaces;

namespace SharedComponents.Models
{
    public class Event : BaseNotifyPropertyChanged, IGameObject
    {
        #region Properties
        private int _Id;

        public int GameId
        {
            get { return _Id; }
            set
            {
                _Id = value;
                NotifyPropertyChanged();
            }
        }

        private string _Name;

        public string Name
        {
            get { return _Name; }
            set
            {
                _Name = value;
                NotifyPropertyChanged();
            }
        }

        private string _Description;

        public string Description
        {
            get { return _Description; }
            set
            {
                _Description = value;
                NotifyPropertyChanged();
            }
        }


        // TODO: Methods
        #endregion
    }
}
