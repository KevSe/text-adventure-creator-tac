﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using SharedComponents.BaseImplementations;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using SharedComponents.Interfaces;
using System.Windows;
using Newtonsoft.Json;

namespace SharedComponents.Models
{
    public class Location : BaseNotifyPropertyChanged
    {
        #region Properties
        private Game _Game;
        public Game Game
        {
            get { return _Game; }
            set { _Game = value; }
        }

        private string _Name;
        public string Name
        {
            get { return _Name; }
            set
            {
                _Name = value;
                NotifyPropertyChanged();
            }
        }

        private string _Description;
        public string Description
        {
            get { return _Description; }
            set
            {
                _Description = value;
                NotifyPropertyChanged();
            }
        }

        private bool _Hidden;
        public bool Hidden
        {
            get { return _Hidden; }
            set
            {
                _Hidden = value;
                NotifyPropertyChanged();
            }
        }

        public bool AlreadyEnteredLocation { get; set; }

        private bool _IsStartLocation;
        public bool IsStartlocation
        {
            get
            {
                return _IsStartLocation;
            }
            set
            {
                if (System.Diagnostics.Process.GetCurrentProcess().ProcessName != "GamePlayer")
                {
                    bool isAlreadyStartlocation = this == this.Game.StartLocation;

                    if (this.Game.StartLocation != null && !isAlreadyStartlocation && value)
                    {
                        MessageBoxResult dialogResult = MessageBox.Show("Another location is already marked as startlocation.\nDo you want to replace it?", "Question", MessageBoxButton.YesNo, MessageBoxImage.Warning);
                            if (dialogResult == MessageBoxResult.No)
                                return;
                    
                        this.Game.StartLocation.IsStartlocation = !value;
                    }
                
                    if (!isAlreadyStartlocation && value)
                        this.Game.StartLocation = this;
                    else if (isAlreadyStartlocation && !value)
                        this.Game.StartLocation = null;

                    _IsStartLocation = value;
                    NotifyPropertyChanged();

                    Console.WriteLine(this.Game.StartLocation != null ? this.Game.StartLocation.Name : "null");
                }
            }
        }

        private ObservableCollection<GameItem> _Items;
        public ObservableCollection<GameItem> Items
        {
            get { return _Items; }
            set
            {
                _Items = value;
                NotifyPropertyChanged();
            }
        }

        private ObservableCollection<Character> _Characters;
        public ObservableCollection<Character> Characters
        {
            get { return _Characters; }
            set
            {
                _Characters = value;
                NotifyPropertyChanged();
            }
        }

        private ObservableCollection<IActionEvent> _FirstTimeEvents;
        public ObservableCollection<IActionEvent> FirstTimeEvents
        {
            get { return _FirstTimeEvents; }
            set
            {
                _FirstTimeEvents = value;
                NotifyPropertyChanged();
            }
        }

        private ObservableCollection<IActionEvent> _EntryEvents;
        public ObservableCollection<IActionEvent> EntryEvents
        {
            get { return _EntryEvents; }
            set
            {
                _EntryEvents = value;
                NotifyPropertyChanged();
            }
        }

        private ObservableCollection<IActionEvent> _ExitEvents;
        public ObservableCollection<IActionEvent> ExitEvents
        {
            get { return _ExitEvents; }
            set
            {
                _ExitEvents = value;
                NotifyPropertyChanged();
            }
        }

        private ObservableCollection<IActionEvent> _GeneralEvents;
        public ObservableCollection<IActionEvent> GeneralEvents
        {
            get { return _GeneralEvents; }
            set
            {
                _GeneralEvents = value;
                NotifyPropertyChanged();
            }
        }

        private ObservableCollection<LocationLink> _Locationslinks;
        public ObservableCollection<LocationLink> LocationLinks
        {
            get { return _Locationslinks; }
            set
            {
                _Locationslinks = value;
                NotifyPropertyChanged();
            }
        }

        private ObservableCollection<IGameCondition> _EnterConditions;
        public ObservableCollection<IGameCondition> EnterConditions
        {
            get { return _EnterConditions; }
            set
            {
                _EnterConditions = value;
                NotifyPropertyChanged();
            }
        }
        #endregion

        #region constructor
        public Location() { }

        public Location(Game game)
        {
            this.Game = game;
            this.FirstTimeEvents = new ObservableCollection<IActionEvent>();
            this.EntryEvents = new ObservableCollection<IActionEvent>();
            this.ExitEvents = new ObservableCollection<IActionEvent>();
            this.GeneralEvents = new ObservableCollection<IActionEvent>();
        }
        #endregion 
    }
}
