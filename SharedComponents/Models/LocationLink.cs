﻿using SharedComponents.BaseImplementations;
using SharedComponents.Interfaces;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedComponents.Models
{
    public class LocationLink : BaseNotifyPropertyChanged
    {
        #region Properties
        private int _Id;
        public int Id
        {
            get { return _Id; }
            set
            {
                _Id = value;
                NotifyPropertyChanged();
            }
        }

        private Location _DestinationLocation;
        public Location DestinationLocation
        {
            get { return _DestinationLocation; }
            set
            {
                _DestinationLocation = value;
                NotifyPropertyChanged();
            }
        }

        private Location _SourceLocation;
        public Location SourceLocation
        {
            get { return _SourceLocation; }
            set
            {
                _SourceLocation = value;
                NotifyPropertyChanged();
            }
        }

        private string _Name;
        public string Name
        {
            get
            {
                return _Name;
            }
            set
            {
                _Name = value;
                NotifyPropertyChanged();
            }
        }

        private string _Description;
        public string Description
        {
            get { return _Description; }
            set
            {
                _Description = value;
                NotifyPropertyChanged();
            }
        }

        private bool _Hidden;
        public bool Hidden
        {
            get { return _Hidden; }
            set
            {
                _Hidden = value;
                NotifyPropertyChanged();
            }
        }

        private ObservableCollection<IGameCondition> _TransferConditions;
        public ObservableCollection<IGameCondition> TransferConditions
        {
            get { return _TransferConditions; }
            set
            {
                _TransferConditions = value;
                NotifyPropertyChanged();
            }
        }

        public bool markedForRemove { get; set; }
        #endregion

        #region Constuctor
        public LocationLink(Location source, Location destination)
        {
            this.SourceLocation = source;
            this.DestinationLocation = destination;
        }
        #endregion

    }
}
