﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedComponents.BaseImplementations;
using SharedComponents.Interfaces;

namespace SharedComponents.Models
{
    public class GameItem : BaseNotifyPropertyChanged
    {
        #region Properties
        public ObservableCollection<IGameCondition> UseConditions { get; set; }
        public ObservableCollection<IActionEvent> ActionEvents { get; set; }

        private int _Id;
        public int Id
        {
            get { return _Id; }
            set
            {
                _Id = value;
                NotifyPropertyChanged();
            }
        }

        private string _Name;
        public string Name
        {
            get { return _Name; }
            set
            {
                _Name = value;
                NotifyPropertyChanged();
            }
        }

        private string _Description;
        public string Description
        {
            get { return _Description; }
            set
            {
                _Description = value;
                NotifyPropertyChanged();
            }
        }

        private bool _IsInventoryObject;
        public bool IsInventoryObject
        {
            get { return _IsInventoryObject; }
            set
            {
                _IsInventoryObject = value;
                NotifyPropertyChanged();
            }
        }
        #endregion
    }
}
