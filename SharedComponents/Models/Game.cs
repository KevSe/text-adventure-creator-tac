﻿using Newtonsoft.Json;
using SharedComponents.BaseImplementations;
using SharedComponents.Interfaces;
using SharedComponents.Models.MethodModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedComponents.Models
{
    public class Game : BaseNotifyPropertyChanged
    {
        #region Properties
        private int _GameId;
        public int GameId
        {
            get { return _GameId; }
            set { _GameId = value; }
        }

        private string _Name;
        public string Name
        {
            get { return _Name; }
            set
            {
                _Name = value;
                NotifyPropertyChanged();
            }
        }

        private string _Description;
        public string Description
        {
            get { return _Description; }
            set
            {
                _Description = value;
                NotifyPropertyChanged();
            }
        }

        private DateTime _LastEditDate;
        public DateTime LastEditDate
        {
            get { return _LastEditDate; }
            set
            {
                _LastEditDate = value;
                NotifyPropertyChanged();
            }
        }
        private Location _StartLocation;
        public Location StartLocation
        {
            get { return _StartLocation; }
            set
            {
                _StartLocation = value;
                NotifyPropertyChanged();
            }
        }
        
        public ObservableCollection<GameVariable> GameVariables { get; set; }
        public ObservableCollection<GameItem> GameItems { get; set; }
        public ObservableCollection<Location> Locations { get; set; }
        public ObservableCollection<Character> Characters { get; set; }
        #endregion


        public Game()
        {
            this.Locations = new ObservableCollection<Location>();
            this.GameItems = new ObservableCollection<GameItem>();
            this.Characters = new ObservableCollection<Character>();
            this.GameVariables = new ObservableCollection<GameVariable>();
        }
        
    }
}
