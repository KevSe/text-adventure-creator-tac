﻿using SharedComponents.Interfaces;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedComponents.Models.MethodModels
{
    public class ActionEvent_Condition : ActionEventBase, IActionEvent
    {
        #region Properties
        public ObservableCollection<IActionEvent> TrueEvents { get; set; }
        public ObservableCollection<IActionEvent> FalseEvents { get; set; }

        private IGameCondition _Condition;
        public IGameCondition Condition
        {
            get { return _Condition; }
            set
            {
                _Condition = value;
                NotifyPropertyChanged();
            }
        }
        #endregion

        #region Constructor
        public ActionEvent_Condition() { }

        public ActionEvent_Condition(IGameCondition condition)
        {
            this.Condition = condition;
            this.Name = $"Condition: {condition.Name}";

            this.TrueEvents = new ObservableCollection<IActionEvent>();
            this.FalseEvents = new ObservableCollection<IActionEvent>();
        }
        #endregion

        #region Methodes
        /// <summary>
        /// Returns a bool
        /// </summary>
        /// <returns></returns>
        public object GetResult()
        {
            return this.Condition.Compare();
        }

        public void Update(IGameCondition condition)
        {
            this.Condition = condition;
            this.Name = $"Condition: {condition.Name}";
        }
        #endregion
    }
}