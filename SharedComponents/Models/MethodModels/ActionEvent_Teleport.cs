﻿using SharedComponents.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedComponents.Models.MethodModels
{
    public class ActionEvent_Teleport : ActionEventBase, IActionEvent
    {
        #region Properties
        private Location _DestinationLocation;
        public Location DestinationLocation
        {
            get { return _DestinationLocation; }
            set
            {
                _DestinationLocation = value;
                NotifyPropertyChanged();
            }
        }
        #endregion

        #region Constructor
        public ActionEvent_Teleport() { }

        public ActionEvent_Teleport(Location location)
        {
            this.DestinationLocation = location;
            this.Name = $"Teleport to: {location.Name}";
        }
        #endregion

        #region Methodes
        public object GetResult()
        {
            return DestinationLocation;
        }

        public void Update(Location location)
        {
            this.DestinationLocation = location;
            this.Name = $"Teleport to: {location.Name}";
        }
        #endregion
    }
}