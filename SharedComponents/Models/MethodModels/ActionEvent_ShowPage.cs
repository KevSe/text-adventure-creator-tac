﻿using SharedComponents.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedComponents.Models.MethodModels
{
    public class ActionEvent_ShowPage : ActionEventBase, IActionEvent
    {
        #region Properties
        private Page _Page;
        public Page Page
        {
            get { return _Page; }
            set
            {
                _Page = value;
                NotifyPropertyChanged();
            }
        }
        #endregion

        #region Constructor
        public ActionEvent_ShowPage() { }

        public ActionEvent_ShowPage(Page page)
        {
            this.Page = page;
            this.Name = page.Text;
        }

        public ActionEvent_ShowPage(string text)
        {
            this.Page = new Page(text);
            this.Name = text;
        }
        #endregion

        #region Methodes
        public object GetResult()
        {
            return Page;
        }

        public void Update(string text)
        {
            this.Page.Text = text;
            this.Name = text;
        }
        #endregion
    }
}
