﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedComponents.Models.MethodModels.Conditions
{
    public enum EVariableOperation
    {
        Add,
        Substract,
        Multiply,
        Devide,
        Set,
        Modulo
    }
}
