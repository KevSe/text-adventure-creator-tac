﻿using SharedComponents.BaseImplementations;
using SharedComponents.Interfaces;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedComponents.Models.MethodModels.Conditions
{
    public class TransferCondition : VariableCondition
    {
        #region Properties
        private ObservableCollection<GameItem> _Items;
        public virtual ObservableCollection<GameItem> Items
        {
            get { return _Items; }
            set
            {
                _Items = value;
                NotifyPropertyChanged();
            }
        }
        #endregion

        public override bool Compare()
        {
            return true;
        }
    }
}
