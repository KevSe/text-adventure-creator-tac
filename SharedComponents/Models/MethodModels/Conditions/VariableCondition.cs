﻿using SharedComponents.BaseImplementations;
using SharedComponents.Interfaces;
using SharedComponents.Models.MethodModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedComponents.Models.MethodModels.Conditions
{
    public class VariableCondition : BaseNotifyPropertyChanged, IGameCondition
    {
        #region Properties
        private int _Id;
        public int ConditionId
        {
            get { return _Id; }
            set
            {
                _Id = value;
                NotifyPropertyChanged();
            }
        }

        public string Name
        {
            get { return $"{FirstValue?.Name ?? ""} {Comparer.ToString() ?? ""} {SecondValue?.Name ?? ""}"; }
        }

        private string _Description;
        public string Description
        {
            get { return _Description; }
            set
            {
                _Description = value;
                NotifyPropertyChanged();
            }
        }

        private GameVariable _FirstValue;
        public GameVariable FirstValue
        {
            get { return _FirstValue; }
            set
            {
                _FirstValue = value;
                NotifyPropertyChanged();
                NotifyPropertyChanged("Name");
            }
        }

        private GameVariable _SecondValue;
        public GameVariable SecondValue
        {
            get { return _SecondValue; }
            set
            {
                _SecondValue = value;
                NotifyPropertyChanged();
                NotifyPropertyChanged("Name");
            }
        }

        private EVariableComparer _Comparer;
        public EVariableComparer Comparer
        {
            get { return _Comparer; }
            set
            {
                _Comparer = value;
                NotifyPropertyChanged();
                NotifyPropertyChanged("Name");
            }
        }
        #endregion

        #region Constructor
        public VariableCondition()
        {
            this.Comparer = EVariableComparer.Equal;
        }
        #endregion

        #region Methods
        public virtual bool Compare()
        {
            switch (Comparer)
            {
                case EVariableComparer.Equal:           return FirstValue.Value == SecondValue.Value;
                case EVariableComparer.NotEqual:        return FirstValue.Value != SecondValue.Value;
                case EVariableComparer.Greater:         return FirstValue.Value >  SecondValue.Value;
                case EVariableComparer.GreaterOrEqual:  return FirstValue.Value >= SecondValue.Value;
                case EVariableComparer.Less:            return FirstValue.Value <  SecondValue.Value;
                case EVariableComparer.LessOrEqual:     return FirstValue.Value <= SecondValue.Value;
            }

            throw new Exception("Can not compare non-existence.");
        }
        #endregion
    }
}
