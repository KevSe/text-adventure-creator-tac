﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedComponents.Models.MethodModels.Conditions
{
    /// <summary>
    /// Belongs to Condition class - because of binding.
    /// </summary>
    public enum EVariableComparer
    {
        Equal,
        Greater,
        Less,
        NotEqual,
        GreaterOrEqual,
        LessOrEqual
    }
}
