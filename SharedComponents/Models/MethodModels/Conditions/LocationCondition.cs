﻿using SharedComponents.BaseImplementations;
using SharedComponents.Interfaces;
using SharedComponents.Models.MethodModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace SharedComponents.Models.MethodModels.Conditions
{
    public class LocationCondition : BaseNotifyPropertyChanged, IGameCondition
    {
        #region Properties
        private int _Id;
        public int ConditionId
        {
            get { return _Id; }
            set
            {
                _Id = value;
                NotifyPropertyChanged();
            }
        }

        public string Name
        {
            get { return $"Compare Location: {CompareLocation?.Name ?? ""}" ; }
        }

        private string _Description;
        public string Description
        {
            get { return _Description; }
            set
            {
                _Description = value;
                NotifyPropertyChanged();
            }
        }

        private Location _Location;
        public Location CompareLocation
        {
            get { return _Location; }
            set
            {
                _Location = value;
                NotifyPropertyChanged();
            }
        }

        private bool _NameEnabled;
        public bool NameEnabled
        {
            get { return _NameEnabled; }
            set
            {
                _NameEnabled = value;
                NotifyPropertyChanged();
            }
        }

        private bool _StartLocationEnabled;
        public bool StartLocationEnabled
        {
            get { return _StartLocationEnabled; }
            set
            {
                _StartLocationEnabled = value;
                NotifyPropertyChanged();
            }
        }

        private bool _HiddenEnabled;
        public bool HiddenEnabled
        {
            get { return _HiddenEnabled; }
            set
            {
                _HiddenEnabled = value;
                NotifyPropertyChanged();
            }
        }

        private string _CompareName;
        public string CompareName
        {
            get { return _CompareName; }
            set
            {
                _CompareName = value;
                NotifyPropertyChanged();
            }
        }

        private bool _CompareIsStartlocation;
        public bool CompareIsStartlocation
        {
            get { return _CompareIsStartlocation; }
            set
            {
                _CompareIsStartlocation = value;
                NotifyPropertyChanged();
            }
        }

        private bool _CompareIsHidden;
        public bool CompareIsHidden
        {
            get { return _CompareIsHidden; }
            set
            {
                _CompareIsHidden = value;
                NotifyPropertyChanged();
            }
        }


        private EVariableComparer _Comparer;
        public EVariableComparer Comparer
        {
            get { return _Comparer; }
            set
            {
                _Comparer = value;
                NotifyPropertyChanged();
                NotifyPropertyChanged("Name");
            }
        }
        #endregion

        #region Constructor
        public LocationCondition()
        {
            this.Comparer = EVariableComparer.Equal;
        }
        #endregion

        #region Methods
        public virtual bool Compare()
        {
            if (CompareLocation == null)
            {
                MessageBox.Show("CompareLocation not deklared. Canceling comparison.");
                return false;
            }

            if (NameEnabled && !String.Equals(CompareLocation.Name, CompareName))
                    return false;

            if (StartLocationEnabled && CompareLocation.IsStartlocation != CompareIsStartlocation)
                return false;

            if (HiddenEnabled && CompareLocation.Hidden != CompareIsHidden)
                return false;

            return true;
        }
        #endregion
    }
}
