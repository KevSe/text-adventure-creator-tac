﻿using SharedComponents.BaseImplementations;
using SharedComponents.Models.MethodModels.Conditions;

namespace SharedComponents.Models.MethodModels
{
    public class GameVariable : BaseNotifyPropertyChanged
    {
        private string _Name;
        public string Name
        {
            get { return _Name; }
            set
            {
                _Name = value;
                NotifyPropertyChanged();
            }
        }

        private string _Description;
        public string Description
        {
            get { return _Description; }
            set
            {
                _Description = value;
                NotifyPropertyChanged();
            }
        }
        
        private int _Value;
        public int Value
        {
            get { return _Value; }
            set
            {
                _Value = value;
                NotifyPropertyChanged();
            }
        }

        public GameVariable() { }
        
        public GameVariable(int value)
        {
            this.Value = value;
        }

        public void Update(int value)
        {
            this.Value = value;
        }
    }
}