﻿using SharedComponents.Interfaces;
using SharedComponents.Models.MethodModels.Conditions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedComponents.Models.MethodModels
{
    public class ActionEvent_ChangeVariable : ActionEventBase, IActionEvent
    {
        #region Properties
        private GameVariable _Variable;
        public GameVariable Variable
        {
            get { return _Variable; }
            set
            {
                _Variable = value;
                NotifyPropertyChanged();
            }
        }

        private EVariableOperation _MathOperationType;
        public EVariableOperation eVariableOperation
        {
            get { return _MathOperationType; }
            set
            {
                _MathOperationType = value;
                NotifyPropertyChanged();
            }
        }

        private GameVariable _Operant;
        public GameVariable Operant
        {
            get { return _Operant; }
            set
            {
                _Operant = value;
                NotifyPropertyChanged();
            }
        }
        #endregion

        #region Constructor
        public ActionEvent_ChangeVariable()
        {
            Variable = new GameVariable();
        }

        public ActionEvent_ChangeVariable(GameVariable gameVariable, EVariableOperation eVariableOperation)
        {
            this.Variable = gameVariable;
            this.eVariableOperation = eVariableOperation;
        }
        #endregion

        #region Methodes
        public object GetResult()
        {
            try
            {
                Change(Operant.Value, eVariableOperation);
                return 1;
            }
            catch
            {
                return 0;
            }
        }

        public void Change(int operantValue, EVariableOperation operation)
        {
            switch (operation)
            {
                case EVariableOperation.Set:
                    Variable.Update(operantValue);
                    break;
                case EVariableOperation.Add:
                    Variable.Update(Variable.Value + operantValue);
                    break;
                case EVariableOperation.Substract:
                    Variable.Update(Variable.Value - operantValue);
                    break;
                case EVariableOperation.Devide:
                    Variable.Update(Variable.Value / operantValue);
                    break;
                case EVariableOperation.Multiply:
                    Variable.Update(Variable.Value * operantValue);
                    break;
                case EVariableOperation.Modulo:
                    Variable.Update(Variable.Value % operantValue);
                    break;
            }
        }
        #endregion
    }
}
