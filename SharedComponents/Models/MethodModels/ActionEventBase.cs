﻿using SharedComponents.BaseImplementations;
using SharedComponents.Models.MethodModels.Conditions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks; 

namespace SharedComponents.Models.MethodModels
{
    public abstract class ActionEventBase : BaseNotifyPropertyChanged
    {
        private int _Id;
        public int Id
        {
            get { return _Id; }
            set
            {
                _Id = value;
                NotifyPropertyChanged();
            }
        }
        
        private string _Name;
        public string Name
        {
            get { return _Name; }
            set
            {
                _Name = value;
                NotifyPropertyChanged();
            }
        }

        private ObservableCollection<VariableCondition> _ActionEventConditions;
        public ObservableCollection<VariableCondition> ActionEventConditions
        {
            get { return _ActionEventConditions; }
            set
            {
                _ActionEventConditions = value;
                NotifyPropertyChanged();
            }
        }
    }
}
