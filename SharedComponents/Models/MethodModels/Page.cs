﻿using SharedComponents.BaseImplementations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedComponents.Models
{
    public class Page : BaseNotifyPropertyChanged
    {
        private int _Id;
        public int Id
        {
            get { return _Id; }
            set
            {
                _Id = value;
                NotifyPropertyChanged();
            }

        }

        private string _Text;
        public string Text
        {
            get { return _Text; }
            set
            {
                _Text = value;
                NotifyPropertyChanged();
            }
        }

        public Page(string text)
        {
            this.Text = text;
        }
    }
}
