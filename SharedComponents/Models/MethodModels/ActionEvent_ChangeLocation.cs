﻿using SharedComponents.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedComponents.Models.MethodModels
{
    public class ActionEvent_ChangeLocation : ActionEventBase, IActionEvent
    {
        #region Properties
        private Location _LocationToChange;
        public Location LocationToChange
        {
            get { return _LocationToChange; }
            set
            {
                _LocationToChange = value;
                NotifyPropertyChanged();
            }
        }

        private bool _IsChangeHidden;
        public bool IsChangeHidden
        {
            get { return _IsChangeHidden; }
            set
            {
                _IsChangeHidden = value;
                NotifyPropertyChanged();
            }
        }

        private bool _IsChangeName;
        public bool IsChangeName
        {
            get { return _IsChangeName; }
            set
            {
                _IsChangeName = value;
                NotifyPropertyChanged();
            }
        }

        private bool _NewHidden;
        public bool NewHidden
        {
            get { return _NewHidden; }
            set
            {
                _NewHidden = value;
                NotifyPropertyChanged();
            }
        }

        private string _NewName;
        public string NewName
        {
            get { return _NewName; }
            set
            {
                _NewName = value;
                NotifyPropertyChanged();
            }
        }
        #endregion

        #region Constructor
        public ActionEvent_ChangeLocation() { }

        public ActionEvent_ChangeLocation(Location locationToChange, bool hidden, string name)
        {
            this.LocationToChange = locationToChange;
            this.NewHidden = hidden;
            this.NewName = name;
        }

        public ActionEvent_ChangeLocation(Location locationToChange)
        {
            this.LocationToChange = locationToChange;
        }
        #endregion

        #region Methodes
        public object GetResult()
        {
            if (IsChangeName) LocationToChange.Name = NewName;
            if (IsChangeHidden) LocationToChange.Hidden = NewHidden;

            return this.LocationToChange;
        }

        public void Update(Location locationToChange, bool hidden, string name)
        {
            this.LocationToChange = locationToChange;
            this.NewHidden = hidden;
            this.NewName = name;
        }
        #endregion
    }
}
