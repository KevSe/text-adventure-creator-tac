﻿using Microsoft.Win32;
using Newtonsoft.Json;
using SharedComponents.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace SharedComponents.JsonComponents
{
    /// <summary>
    /// Functionality for saving / loading the project
    /// </summary>
    public class SaveLoadManager
    {
        public Game LoadGameAsJsonDialog()
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "JSON File (*.json)|*.json";

            if (openFileDialog.ShowDialog() == true)
            {
                string json = File.ReadAllText(openFileDialog.FileName);
                return LoadGameFromJson(json);
            }

            return null;
        }

        public void SaveGameAsJsonDialog(Game game)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "JSON File (*.json)|*.json";

            if (saveFileDialog.ShowDialog() == true)
            {
                var json = GetJson(game);

                File.WriteAllText(saveFileDialog.FileName, json);
            }
        }

        public string GetJson(Game game)
        {
            return JsonConvert.SerializeObject(game, new JsonSerializerSettings()
            {
                PreserveReferencesHandling = PreserveReferencesHandling.All,
                TypeNameHandling = TypeNameHandling.Objects,
                Formatting = Formatting.Indented
            });
        }

        public Game LoadGameFromJson(string json)
        {
            Game game = null;
            try
            {
                game = JsonConvert.DeserializeObject<Game>(json, new JsonSerializerSettings()
                {
                    PreserveReferencesHandling = PreserveReferencesHandling.All,
                    TypeNameHandling = TypeNameHandling.Objects,
                    ObjectCreationHandling = ObjectCreationHandling.Auto
                });

                foreach(var location in game.Locations)
                {
                    location.AlreadyEnteredLocation = false;
                }
            }
            catch
            {
                MessageBox.Show("Something went wrong.\nPlease check, if your file has the right game format.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }

            return game;
        }

        public void SaveAsGameDialog(Game game)
        {
            var saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "TAC Game File (*.tac)|*.tac";

            if (saveFileDialog.ShowDialog() == true)
            {
                var json = new SaveLoadManager().GetJson(game);

                using (var archiveStream = new FileStream(saveFileDialog.FileName, FileMode.Create))
                {
                    using (var archive = new ZipArchive(archiveStream, ZipArchiveMode.Create, true))
                    {
                        var jsonFile = archive.CreateEntry("game.json", CompressionLevel.NoCompression);
                        using (var entryStream = jsonFile.Open())
                        using (var streamWriter = new StreamWriter(entryStream))
                        {
                            streamWriter.Write(json);
                        }
                    }

                }

            }
        }

        public Game LoadGameDialog()
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "TAC Game File (*.tac)|*.tac";

            if (openFileDialog.ShowDialog() == true)
            {
                using (var archive = ZipFile.OpenRead(openFileDialog.FileName))
                {
                    var jsonStream = archive.Entries.FirstOrDefault(x => x.Name == "game.json").Open();

                    using (var sr = new StreamReader(jsonStream))
                    {
                        var jsonString = sr.ReadToEnd();
                        return LoadGameFromJson(jsonString);
                    }
                }
            }
            else
                return null;
        }
    }
}
