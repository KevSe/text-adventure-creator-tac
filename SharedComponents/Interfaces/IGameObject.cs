﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedComponents.Interfaces
{
    public interface IGameObject
    {
        int GameId { get; set; }
        string Name { get; }
        string Description { get; set; }
    }
}
