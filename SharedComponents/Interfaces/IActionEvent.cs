﻿using SharedComponents.Models;
using SharedComponents.Models.MethodModels.Conditions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedComponents.Interfaces
{
    public interface IActionEvent
    {
        int Id { get; set; }
        string Name { get; set; }
        ObservableCollection<VariableCondition> ActionEventConditions { get; set; }
        object GetResult();
    }
}
