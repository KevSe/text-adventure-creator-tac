﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedComponents.Interfaces
{
    public interface IGameCondition
    {
        string Name { get; }
        bool Compare();
    }
}
