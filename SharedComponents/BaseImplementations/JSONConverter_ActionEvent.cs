﻿using Newtonsoft.Json;
using SharedComponents.Models.MethodModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedComponents.BaseImplementations
{
    public class JSONConverter_ActionEvent : JsonConverter
    {
        private static readonly string ShowPage_FULLNAME = typeof(ActionEvent_ShowPage).FullName;
        
        public override bool CanConvert(Type objectType)
        {
            if (objectType.FullName == ShowPage_FULLNAME)
            {
                return true;
            }
            return false;
        }

        public override object ReadJson(Newtonsoft.Json.JsonReader reader, Type objectType, object existingValue, Newtonsoft.Json.JsonSerializer serializer)
        {
            if (objectType.FullName == ShowPage_FULLNAME)
                return serializer.Deserialize(reader, typeof(ActionEvent_ShowPage));

            throw new NotSupportedException(string.Format("Type {0} unexpected.", objectType));
        }

        public override void WriteJson(Newtonsoft.Json.JsonWriter writer, object value, Newtonsoft.Json.JsonSerializer serializer)
        {
            serializer.Serialize(writer, value);
        }
    }
}
