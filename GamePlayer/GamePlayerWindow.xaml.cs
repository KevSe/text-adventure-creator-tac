﻿using GamePlayer.ViewModels;
using SharedComponents.BaseImplementations;
using SharedComponents.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GamePlayer
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class GamePlayerWindow : Window
    {
        public GamePlayerWindow()
        {
            InitializeComponent();
            ConsoleInputBox.Focus();
        }

        public GamePlayerWindow(Game game) : this()
        {
            this.DataContext = new GamePlayerViewModel(game);
        }
    }
}
