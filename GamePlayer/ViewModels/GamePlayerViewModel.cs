﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Microsoft.Win32;
using Newtonsoft.Json;
using SharedComponents.BaseImplementations;
using SharedComponents.Interfaces;
using SharedComponents.JsonComponents;
using SharedComponents.Models;
using SharedComponents.Models.MethodModels;

namespace GamePlayer.ViewModels
{
    public class GamePlayerViewModel : BaseNotifyPropertyChanged
    {
        #region Properties
        public RelayCommand ChooseGameCommand { get; set; }
        public RelayCommand QuitGameCommand { get; set; }
        public RelayCommand ConsolePressEnterCommand { get; set; }

        public ConsoleViewModel ConsoleLogic { get; set; }

        private bool _GameRunning;
        public bool NoGameRunning
        {
            get { return _GameRunning; }
            set
            {
                _GameRunning = value;
                NotifyPropertyChanged();
            }
        }

        private Game _Game;
        public Game Game
        {
            get { return _Game; }
            set
            {
                _Game = value;
                NotifyPropertyChanged();
            }
        }

        public ObservableCollection<Page> PageHistory { get; set; } = new ObservableCollection<Page>();

        public Page CurrentPage
        {
            get { return this.PageHistory.LastOrDefault(); }
        }
        
        private Location _CurrentLocation;
        public Location CurrentLocation
        {
            get { return _CurrentLocation; }
            set
            {
                _CurrentLocation = value;
                NotifyPropertyChanged();
            }
        }
        #endregion

        #region Constructor
        public GamePlayerViewModel()
        {
            this.NoGameRunning = true;
            this.ConsoleLogic = new ConsoleViewModel(this);

            this.ChooseGameCommand = new RelayCommand(ChooseGame);
            this.QuitGameCommand = new RelayCommand(QuitGame);
            this.ConsolePressEnterCommand = new RelayCommand(this.ConsoleLogic.ConsolePressEnter);
        }

        public GamePlayerViewModel(Game game) : this()
        {
            LoadGame(game);
        }
        #endregion

        #region Methodes
        public void ChooseGame()
        {
            SaveLoadManager jsonManager = new SaveLoadManager();
            var game = jsonManager.LoadGameDialog();

            LoadGame(game);
        }

        public void QuitGame()
        {
            System.Windows.Application.Current.Shutdown();
        }

        public void ExecuteActionEvent(IActionEvent actionEvent)
        {
            if (actionEvent is ActionEvent_ShowPage)
            {
                this.PageHistory.Add(actionEvent.GetResult() as Page);
            }
            else if (actionEvent is ActionEvent_Teleport)
            {
                var loc = actionEvent.GetResult() as Location;

                // Execute exit events
                if (CurrentLocation.ExitEvents != null)
                    foreach (var aEvent in CurrentLocation.ExitEvents) ExecuteActionEvent(aEvent);

                // Change location
                this.CurrentLocation = loc;
                this.ConsoleLogic.Console_Write(!String.IsNullOrWhiteSpace(loc.Description) ? loc.Description : $"You did go to {loc.Name}.");

                // Execute first time events
                if (!CurrentLocation.AlreadyEnteredLocation)
                {
                    foreach (var aEvent in CurrentLocation.FirstTimeEvents)
                        ExecuteActionEvent(aEvent);

                    CurrentLocation.AlreadyEnteredLocation = true;
                }

                // Execute entry events
                if (CurrentLocation.EntryEvents != null)
                    foreach (var aEvent in CurrentLocation.EntryEvents) ExecuteActionEvent(aEvent);
            }
            else if (actionEvent is ActionEvent_ChangeVariable variableChange)
            {
                var result = actionEvent.GetResult();
                Debug.WriteLine($"Variable changed: {variableChange.Variable.Name} is now {variableChange.Variable.Value}");                
            }
            else if (actionEvent is ActionEvent_ChangeLocation locationChange)
            {
                var result = actionEvent.GetResult();
                Debug.WriteLine($"Location changed: {locationChange.LocationToChange.Name}");
            }
            else if (actionEvent is ActionEvent_Condition conditionEvent)
            {
                bool result = (conditionEvent.GetResult() as bool?).Value;

                foreach(var aEvent in (result ? conditionEvent.TrueEvents : conditionEvent.FalseEvents))
                    ExecuteActionEvent(aEvent);
            }
        }
        #endregion

        #region Private logic
        private void LoadGame(Game game)
        {
            if (game != null)
            {
                this.Game = game;
                var startLocation = this.Game.StartLocation;

                if (startLocation != null && this.Game.StartLocation.FirstTimeEvents != null)
                {
                    this.CurrentLocation = this.Game.StartLocation;

                    foreach (var actionEvent in CurrentLocation.FirstTimeEvents)
                        this.ExecuteActionEvent(actionEvent);

                    CurrentLocation.AlreadyEnteredLocation = true;
                    this.NoGameRunning = false;
                }
                else
                    MessageBox.Show("No starting page or location found.", "Error while loading first page", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        #endregion
    }
}
