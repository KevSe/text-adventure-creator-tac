﻿using GamePlayer.ViewModels;
using SharedComponents.BaseImplementations;
using SharedComponents.Models;
using SharedComponents.Models.MethodModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace GamePlayer.ViewModels
{
    public class ConsoleViewModel : BaseNotifyPropertyChanged
    {
        private GamePlayerViewModel GamePlayerVM { get; set; }

        private delegate void AfterInputDelegate(string input);
        private AfterInputDelegate AfterInputHandler { get; set; }
        private List<string> ChoosableInputs { get; set; } = new List<string>();
        private bool WaitForInput { get; set; }

        private string _ConsoleInput;
        public string ConsoleInput
        {
            get { return _ConsoleInput; }
            set
            {
                _ConsoleInput = value;
                NotifyPropertyChanged();

                if (value != String.Empty)
                    ConsoleInputCache = value;
            }
        }

        private string ConsoleInputCache { get; set; }

        #region Constructor
        public ConsoleViewModel(GamePlayerViewModel gamePlayerVM)
        {
            this.GamePlayerVM = gamePlayerVM;
        }
        #endregion

        public void ConsolePressEnter()
        {
            if (String.IsNullOrWhiteSpace(this.ConsoleInput))
                GamePlayerVM.PageHistory.Add(new Page("Please enter a text before pressing return."));
            else
            {
                if (this.WaitForInput)
                {
                    if (!this.ChoosableInputs.Contains(this.ConsoleInput))
                    {
                        this.Console_Write("You need to choose something or type cancel, damnit.");
                        this.ConsoleInput = String.Empty;
                        return;
                    }

                    AfterInputHandler.Invoke(this.ConsoleInput);
                    this.WaitForInput = false;
                }
                else
                {
                    string input = ConsoleInput.ToLower();

                    switch (input)
                    {
                        case "go":
                            Go();
                            break;
                        default:
                            GamePlayerVM.PageHistory.Add(new Page("You have done something."));
                            break;
                    }
                }
            }

            this.ConsoleInput = String.Empty;
        }

        public void Console_Write(string output)
        {
            GamePlayerVM.PageHistory.Add(new Page(output));
        }

        public void Console_Clear()
        {
            GamePlayerVM.PageHistory.Clear();
        }
    
        public void Console_WaitForInput(List<string> choosableInputs)
        {
            this.WaitForInput = true;

            if (choosableInputs != null)
                this.ChoosableInputs = choosableInputs;
            else
                this.ChoosableInputs.Clear();
        }

        public void Go()
        {
            if (GamePlayerVM.CurrentLocation.LocationLinks != null && GamePlayerVM.CurrentLocation.LocationLinks.Where(x => !x.DestinationLocation.Hidden).Any())
            {
                var locationLinksDic = new Dictionary<string, LocationLink>();
                var ui_ChoosableLocations = new StringBuilder();
                ui_ChoosableLocations.AppendLine("You have following options:");

                foreach (var locationLink in GamePlayerVM.CurrentLocation.LocationLinks.Where(x => !x.DestinationLocation.Hidden))
                {
                    locationLinksDic.Add(locationLink.Name.ToLower(), locationLink);
                    ui_ChoosableLocations.AppendLine($"\t‣ {locationLink.Name}");
                }

                Console_Write(ui_ChoosableLocations.ToString());

                AfterInputHandler = (string input) =>
                    {
                        ActionEvent_Teleport aeTeleprort = new ActionEvent_Teleport(locationLinksDic[input.ToLower()].DestinationLocation);
                        this.GamePlayerVM.ExecuteActionEvent(aeTeleprort);
                    };
                this.Console_WaitForInput(locationLinksDic.Keys.ToList());
            }
            else
                Console_Write("You do not have anywhere to go.");
        }

    }
}
