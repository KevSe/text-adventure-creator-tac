﻿using SharedComponents.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace SqlDataManager
{
    public class DbManipulation
    {
        public Game SaveChanges(Game game)
        {
            TacDatabaseContext db = null;

            try
            {
                db = new TacDatabaseContext();
                db.Configuration.LazyLoadingEnabled = false;

                var gameResult = db.Games.
                    Include(x => x.Locations)
                    .Include(x => x.Locations.Select(y => y.LocationLinks))
                    .FirstOrDefault(x => game.GameId == x.GameId);

                // Update existing game
                if (gameResult != null)
                {
                    game.UpdateGame(ref db);

                    for (int i = 0; i < gameResult.Locations.Count; i++)
                    {
                        if (gameResult.Locations[i].markedForRemove)
                        {
                            gameResult.Locations.Remove(gameResult.Locations[i]);
                        }

                    }



                    db.SaveChanges();
                    return null;
                }
                // Create new game
                else
                {
                    game.LastEditDate = DateTime.Now;

                    db.Games.Add(game);
                    db.SaveChanges();
                    gameResult = db.Games.SqlQuery("SELECT TOP 1 * FROM Games ORDER BY ID DESC").First();

                    return gameResult;
                }

            }
            catch
            {
                return null;
            }
            finally
            {
                db.Dispose();
            }
        }

       
      

        public Game AddGameToDb(Game game)
        {
            try
            {
                using (var db = new TacDatabaseContext())
                {
                    game.LastEditDate = DateTime.Now;

                    db.Games.Add(game);
                    db.SaveChanges();
                    var newGame = db.Games.SqlQuery("SELECT TOP 1 * FROM Games ORDER BY ID DESC").First();

                    return newGame;
                }
            }
            catch
            {
                throw;
            }
        }

        public void DeleteGameFromDb(int gameId)
        {
            try
            {
                using (var db = new TacDatabaseContext())
                {
                    var gameToDelete = db.Games.Where(x => x.GameId == gameId).First();

                    db.Games.Remove(gameToDelete);

                    db.SaveChanges();
                }
            } 
            catch
            {
                throw;
            }
        }

        public void DeleteLocationFromDb(int locationId)
        {
            try
            {
                using (var db = new TacDatabaseContext())
                {
                    var locationToDelete = db.Locations.Where(x => x.Id == locationId).FirstOrDefault();

                    if (locationToDelete != null)
                        db.Locations.Remove(locationToDelete);

                    db.SaveChanges();
                }
            }
            catch
            {
                throw;
            }
        }


        public ObservableCollection<Game> GetGamesFromDb()
        {
            ObservableCollection<Game> games = null;

            using (var db = new TacDatabaseContext())
            {
                db.Configuration.LazyLoadingEnabled = false;


                var result = db.Games
                    .Include(x => x.Locations)
                    .Include(x => x.Locations.Select(y => y.LocationLinks));
                //var result = db.Games.IncludeAll();
                if (result != null)
                {
                    games = new ObservableCollection<Game>();

                    foreach (Game game in result)
                    {
                        games.Add(game);
                    }
                }
            }

            return games;
        }
    }
}
