namespace SqlDataManager.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class newmig : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Conditions",
                c => new
                    {
                        ConditionId = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Description = c.String(),
                        FirstValue = c.Int(nullable: false),
                        SecondValue = c.Int(nullable: false),
                        Comparer = c.Int(nullable: false),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                        Location_LocationId = c.Int(),
                    })
                .PrimaryKey(t => t.ConditionId)
                .ForeignKey("dbo.Locations", t => t.Location_LocationId)
                .Index(t => t.Location_LocationId);
            
            CreateTable(
                "dbo.LocationLinks",
                c => new
                    {
                        LocationId = c.Int(nullable: false),
                        SourceLocationId = c.Int(nullable: false),
                        Description = c.String(),
                        Hidden = c.Boolean(nullable: false),
                        markedForRemove = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => new { t.LocationId, t.SourceLocationId })
                .ForeignKey("dbo.Locations", t => t.LocationId, cascadeDelete: true)
                .ForeignKey("dbo.Locations", t => t.SourceLocationId, cascadeDelete: true)
                .Index(t => t.LocationId)
                .Index(t => t.SourceLocationId);
            
            CreateTable(
                "dbo.Locations",
                c => new
                    {
                        LocationId = c.Int(nullable: false, identity: true),
                        GameId = c.Int(nullable: false),
                        Name = c.String(),
                        Description = c.String(),
                        Hidden = c.Boolean(nullable: false),
                        markedForRemove = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.LocationId)
                .ForeignKey("dbo.Games", t => t.GameId, cascadeDelete: true)
                .Index(t => t.GameId);
            
            CreateTable(
                "dbo.Characters",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Description = c.String(),
                        Game_GameId = c.Int(),
                        Location_LocationId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Games", t => t.Game_GameId)
                .ForeignKey("dbo.Locations", t => t.Location_LocationId)
                .Index(t => t.Game_GameId)
                .Index(t => t.Location_LocationId);
            
            CreateTable(
                "dbo.Games",
                c => new
                    {
                        GameId = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Description = c.String(),
                        LastEditDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.GameId);
            
            CreateTable(
                "dbo.Items",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Description = c.String(),
                        Character_Id = c.Int(),
                        Location_LocationId = c.Int(),
                        Location_LocationId1 = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Characters", t => t.Character_Id)
                .ForeignKey("dbo.Locations", t => t.Location_LocationId)
                .ForeignKey("dbo.Locations", t => t.Location_LocationId1)
                .Index(t => t.Character_Id)
                .Index(t => t.Location_LocationId)
                .Index(t => t.Location_LocationId1);
            
            CreateTable(
                "dbo.LocationLink_TransferCondition",
                c => new
                    {
                        LocationId = c.Int(nullable: false),
                        SourceLocationId = c.Int(nullable: false),
                        ConditionId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.LocationId, t.SourceLocationId, t.ConditionId })
                .ForeignKey("dbo.LocationLinks", t => new { t.LocationId, t.SourceLocationId }, cascadeDelete: true)
                .ForeignKey("dbo.Conditions", t => t.ConditionId, cascadeDelete: true)
                .Index(t => new { t.LocationId, t.SourceLocationId })
                .Index(t => t.ConditionId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.LocationLink_TransferCondition", "ConditionId", "dbo.Conditions");
            DropForeignKey("dbo.LocationLink_TransferCondition", new[] { "LocationId", "SourceLocationId" }, "dbo.LocationLinks");
            DropForeignKey("dbo.LocationLinks", "SourceLocationId", "dbo.Locations");
            DropForeignKey("dbo.LocationLinks", "LocationId", "dbo.Locations");
            DropForeignKey("dbo.Items", "Location_LocationId1", "dbo.Locations");
            DropForeignKey("dbo.Items", "Location_LocationId", "dbo.Locations");
            DropForeignKey("dbo.Conditions", "Location_LocationId", "dbo.Locations");
            DropForeignKey("dbo.Characters", "Location_LocationId", "dbo.Locations");
            DropForeignKey("dbo.Items", "Character_Id", "dbo.Characters");
            DropForeignKey("dbo.Locations", "GameId", "dbo.Games");
            DropForeignKey("dbo.Characters", "Game_GameId", "dbo.Games");
            DropIndex("dbo.LocationLink_TransferCondition", new[] { "ConditionId" });
            DropIndex("dbo.LocationLink_TransferCondition", new[] { "LocationId", "SourceLocationId" });
            DropIndex("dbo.Items", new[] { "Location_LocationId1" });
            DropIndex("dbo.Items", new[] { "Location_LocationId" });
            DropIndex("dbo.Items", new[] { "Character_Id" });
            DropIndex("dbo.Characters", new[] { "Location_LocationId" });
            DropIndex("dbo.Characters", new[] { "Game_GameId" });
            DropIndex("dbo.Locations", new[] { "GameId" });
            DropIndex("dbo.LocationLinks", new[] { "SourceLocationId" });
            DropIndex("dbo.LocationLinks", new[] { "LocationId" });
            DropIndex("dbo.Conditions", new[] { "Location_LocationId" });
            DropTable("dbo.LocationLink_TransferCondition");
            DropTable("dbo.Items");
            DropTable("dbo.Games");
            DropTable("dbo.Characters");
            DropTable("dbo.Locations");
            DropTable("dbo.LocationLinks");
            DropTable("dbo.Conditions");
        }
    }
}
