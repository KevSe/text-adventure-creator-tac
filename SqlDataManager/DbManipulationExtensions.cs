﻿using SharedComponents.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SqlDataManager
{
    public static class DbManipulationExtensions
    {
        public static IQueryable<T> IncludeAll<T>(this IQueryable<T> queryable) where T : class
        {
            var type = typeof(T);
            var properties = type.GetProperties();
            foreach (var property in properties)
            {
                var isVirtual = property.GetGetMethod().IsVirtual;
                if (isVirtual && properties.FirstOrDefault(c => c.Name == property.Name + "_Id" || c.Name == property.Name + "_ID") != null)
                {
                    //queryable = queryable.Include(property.Name);
                    Type typexxx = property.PropertyType;
                    queryable = queryable.Include(property.Name).IncludeAll();
                }

            }
            return queryable;
        }


        public static void UpdateGame (this Game game, ref TacDatabaseContext db) 
        {
            var resultGame = db.Games.Find(game.GameId);

            if (resultGame != null)
            {
                resultGame.Name = game.Name;
                resultGame.Description = game.Description;
                resultGame.LastEditDate = DateTime.Now;

                foreach (Location location in game.Locations)
                {
                    location.UpdateLocation(ref db, game.GameId);
                }
            }
        }

        public static void UpdateLocation(this Location location, ref TacDatabaseContext db, int gameId)
        {
            var resultLocation = db.Locations.Find(location.Id);
            
            foreach (LocationLink linkedLocation in location.LocationLinks)
            {
                LocationLink link = linkedLocation.UpdateLinkedLocation(ref db, location.Id, gameId);

                if (link != null)
                {
                    location.LocationLinks.Add(link);
                }
            }

            if (resultLocation != null)
            {
                resultLocation.Name = location.Name;
                resultLocation.Description = location.Description;   
            }
            else
            {
                db.Games.Find(gameId).Locations.Add(location);
            }
        }

        public static LocationLink UpdateLinkedLocation (this LocationLink linkedLocation, ref TacDatabaseContext db, int locationId, int gameId)
        {
            return null;
            //var resultLinkedLocation = db.LocationLinks.Find(linkedLocation.LocationId, linkedLocation.SourceLocationId);

            //if (resultLinkedLocation != null)
            //{
            //    resultLinkedLocation.Hidden = linkedLocation.Hidden;
            //    resultLinkedLocation.Description = linkedLocation.Description;

            //    return null;
            //}
            //else
            //{
            //    return new LocationLink()
            //    {
            //        Description = linkedLocation.Description,
            //        Location = linkedLocation.Location,
            //        SourceLocation = linkedLocation.SourceLocation,
            //        Hidden = linkedLocation.Hidden,
            //        TransferConditions = linkedLocation.TransferConditions
            //    };  
            //}
        }
    }
}
