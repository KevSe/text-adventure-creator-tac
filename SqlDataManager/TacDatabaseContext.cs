﻿namespace SqlDataManager
{
    using System;
    using System.Data.Entity;
    using System.Linq;
    using SharedComponents.Models;

    public class TacDatabaseContext : DbContext
    {
        public TacDatabaseContext()
            : base("name=TacDatabaseContext")
        {
        }

        public virtual DbSet<Game> Games { get; set; }
        public virtual DbSet<Location> Locations { get; set; }
        public virtual DbSet<LocationLink> LocationLinks { get; set; }
        public virtual DbSet<Condition> Conditions { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //modelBuilder.Entity<Game>()
            //.HasMany(x => x.Locations).WithRequired(x => x.Game).HasForeignKey(x => x.GameId).WillCascadeOnDelete(true);

            //modelBuilder.Entity<Location>()
            //    .HasMany(x => x.LocationLinks).WithRequired(x => x.Location).HasForeignKey(x => x.LocationId);

            //modelBuilder.Entity<LocationLink>()
            //    .HasKey(x => x.LocationLinkId);

            //modelBuilder.Entity<LocationLink>()
            //    .HasRequired(x => x.SourceLocation)
            //    .WithOptional()
            //    .WillCascadeOnDelete(false);
                

            //modelBuilder.Entity<LocationLink>()
            //    .HasMany(x => x.TransferConditions).WithMany(x => x.LocationLinks)
            //    .Map(x =>
            //    {
            //        x.ToTable("LocationLink_TransferCondition");
            //        x.MapLeftKey("LocationLinkId");
            //        x.MapRightKey("ConditionId");
            //    });
            

            //-------------- Old code


            //    modelBuilder.Entity<Game>()
            //        .HasKey(s => s.Id)
            //        .HasMany(s => s.Locations)
            //        .WithOptional()
            //        .WillCascadeOnDelete();

            //    modelBuilder.Entity<Location>()
            //        .HasKey(s => s.Id)
            //        .HasMany(s => s.LocationsLinks);

            //    modelBuilder.Entity<LocationLink>()
            //        .HasKey(s => s.Id)
            //        .HasRequired(s => s.Location)
            //        .WithOptional()
            //        .WillCascadeOnDelete(false);

            //    modelBuilder.Entity<LocationLink>()
            //        .HasRequired(s => s.SourceLocation)
            //        .WithRequiredDependent();

        }
    }

}