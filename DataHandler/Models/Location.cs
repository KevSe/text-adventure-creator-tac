﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;

namespace DataHandler.Models
{
    public class Location
    {
        private int _id;

        public int id
        {
            get { return _id; }
            set { _id = value; }
        }

        private string _name;

        public string name
        {
            get { return _name; }
            set { _name = value; }
        }

        private string _description;

        public string description
        {
            get { return _description; }
            set { _description = value; }
        }

        private ObservableCollection<Item> _enviromentItems;

        public ObservableCollection<Item> enviromentItems
        {
            get { return _enviromentItems; }
            set { _enviromentItems = value; }
        }

        private ObservableCollection<Item> _inventoryItems;

        public ObservableCollection<Item> inventoryItems
        {
            get { return _inventoryItems; }
            set { _inventoryItems = value; }
        }

        private ObservableCollection<Character> _characters;

        public ObservableCollection<Character> characters
        {
            get { return _characters; }
            set { _characters = value; }
        }

        private ObservableCollection<Location> _location;

        public ObservableCollection<Location> location
        {
            get { return _location; }
            set { _location = value; }
        }


    }
}
