﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataHandler.Models
{
    public class Game
    {
        #region Properties
        private int _ID;

        public int ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        private string _GameName;

        public string GameName
        {
            get { return _GameName; }
            set { _GameName = value; }
        }

        private List<Location> _Locations;

        public List<Location> Locations
        {
            get { return _Locations; }
            set { _Locations = value; }
        }

        private List<Character> _Characters;

        public List<Character> Charakteres
        {
            get { return _Characters; }
            set { _Characters = value; }
        }


        #endregion
    }
}
