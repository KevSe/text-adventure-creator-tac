﻿namespace SqlDataHandler
{
    using System;
    using System.Data.Entity;
    using System.Linq;
    using SharedComponents.Models;

    public class DbModel : DbContext
    {
        // Der Kontext wurde für die Verwendung einer DbModel-Verbindungszeichenfolge aus der 
        // Konfigurationsdatei ('App.config' oder 'Web.config') der Anwendung konfiguriert. Diese Verbindungszeichenfolge hat standardmäßig die 
        // Datenbank 'DataHandler.DbModel' auf der LocalDb-Instanz als Ziel. 
        // 
        // Wenn Sie eine andere Datenbank und/oder einen anderen Anbieter als Ziel verwenden möchten, ändern Sie die DbModel-Zeichenfolge 
        // in der Anwendungskonfigurationsdatei.
        public DbModel()
            : base("name=DbModel")
        {
        }

        // Fügen Sie ein 'DbSet' für jeden Entitätstyp hinzu, den Sie in das Modell einschließen möchten. Weitere Informationen 
        // zum Konfigurieren und Verwenden eines Code First-Modells finden Sie unter 'http://go.microsoft.com/fwlink/?LinkId=390109'.

        public virtual DbSet<Game> Games { get; set; }
        public virtual DbSet<Location> Locations { get; set; }
        public virtual DbSet<Character> Characters { get; set; }
    }

    //public class MyEntity
    //{
    //    public int Id { get; set; }
    //    public string Name { get; set; }
    //}
}