﻿using SharedComponents.BaseImplementations;
using SharedComponents.Models;
using SharedComponents.Models.MethodModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Documents;

namespace TextAdventureCreator.ViewModels
{
    public class TextEditorViewModel : BaseNotifyPropertyChanged, IDialogRequestClose
    {
        #region Properties
        public event EventHandler<DialogCloseRequestedEventArgs> CloseRequested;
        public RelayCommand OkCommand { get; private set; }
        public RelayCommand CancelCommand { get; private set; }
        public RelayCommand TextBoxPressEnterCommand { get; private set; }

        private string _Text;     
        public string Text
        {
            get { return _Text; }
            set
            {
                _Text = value;
                NotifyPropertyChanged();
            }
        }
        #endregion

        #region Constructor
        public TextEditorViewModel()
        {
            OkCommand = new RelayCommand(p => CloseRequested?.Invoke(this, new DialogCloseRequestedEventArgs(true)));
            CancelCommand = new RelayCommand(p => CloseRequested?.Invoke(this, new DialogCloseRequestedEventArgs(false)));
            TextBoxPressEnterCommand = new RelayCommand(TextBoxPressEnter);
        }

        public TextEditorViewModel(string inlines) : this()
        {
            this.Text = inlines;
        }
        #endregion

        #region Commands
        public ActionEvent_ShowPage GetActionEventShowPage()
        {
            var text = this.Text ?? "[No data input]";
            return new ActionEvent_ShowPage(text);
        }

        public void TextBoxPressEnter()
        {
            if (!String.IsNullOrWhiteSpace(this.Text))
                CloseRequested?.Invoke(this, new DialogCloseRequestedEventArgs(true));
            else
                MessageBox.Show("Please enter anything into the textbox.");
        }
        #endregion

    }
}
