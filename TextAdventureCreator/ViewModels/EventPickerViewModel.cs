﻿using SharedComponents.BaseImplementations;
using SharedComponents.Interfaces;
using SharedComponents.Models;
using SharedComponents.Models.MethodModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TextAdventureCreator.Models;

namespace TextAdventureCreator.ViewModels
{
    class EventPickerViewModel : BaseNotifyPropertyChanged, IDialogRequestClose
    {
        public event EventHandler<DialogCloseRequestedEventArgs> CloseRequested;
        public RelayCommand OkCommand { get; private set; }
        public RelayCommand CancelCommand { get; private set; }
        public RelayCommand InvokeActionEventEditorCommand { get; private set; }

        #region Properties
        public enum EActionEventType
        {
            ShowPage,
            ChangeVariable,
            ChangeLocation,
            Teleport,
            Condition,
        }

        public EActionEventType ActionEventType { get; private set; }
        #endregion

        #region Costructor
        public EventPickerViewModel()
        {
            OkCommand = new RelayCommand(Ok);
            CancelCommand = new RelayCommand(p => CloseRequested?.Invoke(this, new DialogCloseRequestedEventArgs(false)));
        }
        #endregion

        #region Commands
        public void Ok(object buttonPressed)
        {
            string actionEventNameString = (buttonPressed as string).ToLower();
            
            switch(actionEventNameString)
            {
                case "showpage":
                    this.ActionEventType = EActionEventType.ShowPage;
                    break;
                case "teleport":
                    this.ActionEventType = EActionEventType.Teleport;
                    break;
                case "condition":
                    this.ActionEventType = EActionEventType.Condition;
                    break;
                case "variable":
                    this.ActionEventType = EActionEventType.ChangeVariable;
                    break;
                case "location":
                    this.ActionEventType = EActionEventType.ChangeLocation;
                    break;
            }

            CloseRequested?.Invoke(this, new DialogCloseRequestedEventArgs(true));
        }
        #endregion

    }
}
