﻿using SharedComponents.BaseImplementations;
using SharedComponents.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextAdventureCreator.ViewModels
{
    class SelectLocationViewModel : BaseNotifyPropertyChanged, IDialogRequestClose
    {
        public event EventHandler<DialogCloseRequestedEventArgs> CloseRequested;
        public RelayCommand OkCommand { get; private set; }
        public RelayCommand CancelCommand { get; private set; }

        #region Properties

        private ObservableCollection<Location> _AvailableLocations;
        public ObservableCollection<Location> AvailableLocations
        {
            get { return _AvailableLocations; }
            set
            {
                _AvailableLocations = value;
                NotifyPropertyChanged();
            }
        }

        private Location _SelectedLocation;
        public Location SelectedLocation
        {
            get { return _SelectedLocation; }
            set
            {
                _SelectedLocation = value;
                NotifyPropertyChanged();
            }
        }
        #endregion

        #region Constructor
        public SelectLocationViewModel(ObservableCollection<Location> locationsToChooseFrom)
        {
            this.AvailableLocations = locationsToChooseFrom;
            this.SelectedLocation = AvailableLocations.LastOrDefault();

            OkCommand = new RelayCommand(p => CloseRequested?.Invoke(this, new DialogCloseRequestedEventArgs(true)));
            CancelCommand = new RelayCommand(p => CloseRequested?.Invoke(this, new DialogCloseRequestedEventArgs(false)));
        }
        #endregion

    }
}
