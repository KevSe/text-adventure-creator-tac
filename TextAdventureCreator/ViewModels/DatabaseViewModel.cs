﻿using SharedComponents.BaseImplementations;
using SharedComponents.Models;
using SharedComponents.Models.MethodModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextAdventureCreator.ViewModels
{
    public class DatabaseViewModel : BaseNotifyPropertyChanged, IDialogRequestClose
    {
        public readonly IDialogService DialogService;
        public event EventHandler<DialogCloseRequestedEventArgs> CloseRequested;
        public RelayCommand AddGameVariableCommand { get; private set; }
        public RelayCommand DeleteGameVariableCommand { get; private set; }

        public RelayCommand InvokeEventPickerDialogCommand { get; set; }

        #region Properties
        private Game _Game;
        public Game Game
        {
            get { return _Game; }
            set
            {
                _Game = value;
                NotifyPropertyChanged();
            }
        }

        private GameVariable _SelectedGameVariable;
        public GameVariable SelectedGameVariable
        {
            get { return _SelectedGameVariable; }
            set
            {
                _SelectedGameVariable = value;
                NotifyPropertyChanged();
            }
        }
        #endregion

        #region Constructor
        public DatabaseViewModel() { }

        public DatabaseViewModel(Game game)
        {
            this.Game = game;
            this.AddGameVariableCommand = new RelayCommand(AddGameVariable);
            this.DeleteGameVariableCommand = new RelayCommand(DeleteGameVariable);
        }
        #endregion

        #region Methodes
        private void AddGameVariable()
        {
            GameVariable gameVariable = new GameVariable()
            {
                Name = $"Variable {this.Game.GameVariables.Count + 1}"
            };

            this.Game.GameVariables.Add(gameVariable);
        }

        private void DeleteGameVariable()
        {
            if (this.SelectedGameVariable == null)
                return;

            this.Game.GameVariables.Remove(this.SelectedGameVariable);
        }
        #endregion
    }
}
