﻿using SharedComponents.BaseImplementations;
using SharedComponents.Models;
using SharedComponents.Models.MethodModels;
using SharedComponents.Models.MethodModels.Conditions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextAdventureCreator.ViewModels.ConditionEditor
{
    public class LocationConditionVM : BaseNotifyPropertyChanged
    {
        public ObservableCollection<Location> Locations { get; set; }

        private LocationCondition _LocationCondition;
        public LocationCondition Condition
        {
            get { return _LocationCondition; }
            set
            {
                _LocationCondition = value;
                NotifyPropertyChanged();
            }
        }
        
        public LocationConditionVM()
        {
            this.Condition = new LocationCondition();
        }
        
        public bool CanExecuteOk()
        {
            return (this.Condition != null
                && this.Condition.CompareLocation != null
                && (
                       this.Condition.HiddenEnabled
                       || (this.Condition.NameEnabled && !String.IsNullOrWhiteSpace(this.Condition.CompareName))
                       || this.Condition.StartLocationEnabled
                   )
                );
        }

    }
}
