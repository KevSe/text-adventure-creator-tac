﻿using SharedComponents.BaseImplementations;
using SharedComponents.Models;
using SharedComponents.Models.MethodModels;
using SharedComponents.Models.MethodModels.Conditions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextAdventureCreator.ViewModels.ConditionEditor
{
    public class VariableConditionVM : BaseNotifyPropertyChanged
    {
        public ObservableCollection<GameVariable> GameVariables { get; set; }

        private int _SelectedSecondVariableIndex;
        public int SelectedSecondVariableIndex
        {
            get { return _SelectedSecondVariableIndex; }
            set
            {
                SelectedSecondVariableTypeChanged(value);

                _SelectedSecondVariableIndex = value;
                NotifyPropertyChanged();
            }
        }

        private VariableCondition _Condition;
        public VariableCondition Condition
        {
            get { return _Condition; }
            set
            {
                _Condition = value;
                NotifyPropertyChanged();
            }
        }

        private GameVariable _SecondValueFromDb;
        public GameVariable SecondValueFromDb
        {
            get { return _SecondValueFromDb; }
            set
            {
                _SecondValueFromDb = value;
                NotifyPropertyChanged();

                if (Condition.SecondValue != value)
                    Condition.SecondValue = value;
            }
        }

        private GameVariable _SecondValueFromTextbox;
        public GameVariable SecondValueFromTextbox
        {
            get { return _SecondValueFromTextbox; }
            set
            {
                _SecondValueFromTextbox = value;
                NotifyPropertyChanged();

                if (Condition.SecondValue != value)
                    Condition.SecondValue = value;
            }
        }

        private int _SecondValueInput;
        public int SecondValueInput
        {
            get { return _SecondValueInput; }
            set
            {
                _SecondValueInput = value;
                NotifyPropertyChanged();

                SecondValueFromTextbox = new GameVariable() { Name = value.ToString(), Value = value };
            }
        }

        public VariableConditionVM()
        {
            this.Condition = new VariableCondition();

            this.SecondValueFromDb = new GameVariable();
            this.SecondValueFromTextbox = new GameVariable();

            this.Condition.SecondValue = SecondValueFromDb;
        }

        public void SelectedSecondVariableTypeChanged(object index)
        {
            int? indexInt = index as int?;

            if (indexInt != null)
            {
                // GameVariable from db
                if (indexInt == 0)
                    this.Condition.SecondValue = SecondValueFromDb;
                // GameVariable from textbox
                else if (indexInt == 1)
                    this.Condition.SecondValue = SecondValueFromTextbox;
            }
        }

        public bool CanExecuteOk()
        {
            return (this.Condition != null
                && this.Condition.FirstValue != null
                && this.Condition.SecondValue != null);
        }

    }
}
