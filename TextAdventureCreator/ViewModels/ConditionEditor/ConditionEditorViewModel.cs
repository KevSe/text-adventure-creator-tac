﻿using SharedComponents.BaseImplementations;
using SharedComponents.Interfaces;
using SharedComponents.Models;
using SharedComponents.Models.MethodModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using TextAdventureCreator.ViewModels.ConditionEditor;

namespace TextAdventureCreator.ViewModels
{
    public class ConditionEditorViewModel : BaseNotifyPropertyChanged, IDialogRequestClose
    {
        #region Properties
        public event EventHandler<DialogCloseRequestedEventArgs> CloseRequested;
        public RelayCommand OkCommand { get; private set; }
        public RelayCommand CancelCommand { get; private set; }

        private int _ConditionTypeIndex;
        public int ConditionTypeIndex
        {
            get { return _ConditionTypeIndex; }
            set
            {
                _ConditionTypeIndex = value;
                NotifyPropertyChanged();

                SelectedConditionTypeChanged(value);
            }
        }

        public VariableConditionVM CaseVariableCondition { get; set; } = new VariableConditionVM();
        public LocationConditionVM CaseLocationCondition { get; set; } = new LocationConditionVM();
        
        private IGameCondition _Condition;
        public IGameCondition Condition
        {
            get { return _Condition; }
            set
            {
                _Condition = value;
                NotifyPropertyChanged();
            }
        }
        #endregion

        #region Constructor
        public ConditionEditorViewModel() { }
        public ConditionEditorViewModel(Game game)
        {
            this.OkCommand = new RelayCommand(p => CloseRequested?.Invoke(this, new DialogCloseRequestedEventArgs(true)), CanExecuteOk);
            this.CancelCommand = new RelayCommand(p => CloseRequested?.Invoke(this, new DialogCloseRequestedEventArgs(false)));

            this.CaseVariableCondition.GameVariables = game.GameVariables;
            this.CaseLocationCondition.Locations = game.Locations;
            this.Condition = CaseVariableCondition.Condition;
        }
        #endregion

        #region Methodes
        private bool CanExecuteOk(object placeholder)
        {
            if (ConditionTypeIndex == 0)
            {
                return CaseVariableCondition.CanExecuteOk();
            }
            else if (ConditionTypeIndex == 1)
            {
                return CaseLocationCondition.CanExecuteOk();
            }

            return false;
        }


        public void SelectedConditionTypeChanged(object index)
        {
            int? indexInt = index as int?;

            if (indexInt != null)
            {
                // Variable condition
                if (indexInt == 0)
                    this.Condition = CaseVariableCondition.Condition;
                // TODO: Location condition
                else if (indexInt == 1)
                    this.Condition = CaseLocationCondition.Condition;
            }
        }
        #endregion
    }
}
