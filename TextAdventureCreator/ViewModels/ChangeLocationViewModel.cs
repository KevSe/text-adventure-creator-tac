﻿using SharedComponents.BaseImplementations;
using SharedComponents.Models;
using SharedComponents.Models.MethodModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextAdventureCreator.ViewModels
{
    class ChangeLocationViewModel : BaseNotifyPropertyChanged, IDialogRequestClose
    {
        #region Properties
        public event EventHandler<DialogCloseRequestedEventArgs> CloseRequested;
        public RelayCommand OkCommand { get; private set; }
        public RelayCommand CancelCommand { get; private set; }

        public ObservableCollection<Location> ChoosableLocations { get; set; }

        private ActionEvent_ChangeLocation _ActionEvent_ChangeLocation;
        public ActionEvent_ChangeLocation ActionEvent_ChangeLocation
        {
            get { return _ActionEvent_ChangeLocation; }
            set
            {
                _ActionEvent_ChangeLocation = value;
                NotifyPropertyChanged();
            }
        }
        #endregion

        #region Construcor
        public ChangeLocationViewModel()
        {
            OkCommand = new RelayCommand(Ok, CanExecuteOk);
            CancelCommand = new RelayCommand(p => CloseRequested?.Invoke(this, new DialogCloseRequestedEventArgs(false)));
        }

        public ChangeLocationViewModel(ObservableCollection<Location> choosableLocations) : this()
        {
            this.ChoosableLocations = choosableLocations;
            this.ActionEvent_ChangeLocation = new ActionEvent_ChangeLocation();
        }

        public ChangeLocationViewModel(ObservableCollection<Location> choosableLocations, Location locationToChange, string name, bool hidden) : this()
        {
            this.ChoosableLocations = choosableLocations;
            this.ActionEvent_ChangeLocation = new ActionEvent_ChangeLocation(locationToChange, hidden, name);
        }
        #endregion

        #region Methodes
        private void Ok()
        {
            ActionEvent_ChangeLocation.Name = String.Format("{0}{1}{2}",
                ActionEvent_ChangeLocation.IsChangeHidden ? $"Change hiddenflag of location {ActionEvent_ChangeLocation.LocationToChange.Name} to {ActionEvent_ChangeLocation.NewHidden}" : "",
                ActionEvent_ChangeLocation.IsChangeHidden && ActionEvent_ChangeLocation.IsChangeName ? Environment.NewLine : "",
                ActionEvent_ChangeLocation.IsChangeName ? $"Change name of location {ActionEvent_ChangeLocation.LocationToChange.Name} to {ActionEvent_ChangeLocation.NewName}" : "");
                
            CloseRequested?.Invoke(this, new DialogCloseRequestedEventArgs(true));
        }

        private bool CanExecuteOk(object obj)
        {
            return ActionEvent_ChangeLocation?.LocationToChange != null;
        }
        #endregion
    }
}
