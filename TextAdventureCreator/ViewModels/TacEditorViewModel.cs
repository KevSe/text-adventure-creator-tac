﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using SharedComponents.Models;
using SharedComponents.BaseImplementations;
using System.Windows;
using SharedComponents.Interfaces;
using SharedComponents.Models.MethodModels;
using SharedComponents.JsonComponents;

namespace TextAdventureCreator.ViewModels
{
    public class TacEditorViewModel : BaseNotifyPropertyChanged
    {
        #region Properties
        public readonly IDialogService dialogService;
        public RelayCommand PlayGameCommand { get; private set; }
        public RelayCommand OpenDatabaseWindowCommand { get; private set; }
        public RelayCommand AddLocationCommand { get; private set; }
        public RelayCommand SaveCommand { get; private set; }
        public RelayCommand OpenProjectWindowCommand { get; private set; }
        public RelayCommand DeleteLocationCommand { get; private set; }
        public RelayCommand DeleteLocationLinkCommand { get; private set; }
        public RelayCommand AddLocationLinkCommand { get; private set; }
        public RelayCommand SaveAsJsonDialogCommand { get; private set; }
        public RelayCommand LoadJsonDialogCommand { get; private set; }
        public RelayCommand InvokeEventPickerDialogCommand { get; private set; }
        public RelayCommand DeleteActionEventFromLocationCommand { get; private set; }
        public RelayCommand EditActionEventCommand { get; private set; }

        

        private Game _Game;
        public Game Game
        {
            get { return _Game; }
            set
            {
                _Game = value;
                NotifyPropertyChanged();
            }
        }

        private Location _SelectedLocation;
        public Location SelectedLocation
        {
            get { return _SelectedLocation; }
            set
            {
                _SelectedLocation = value;
                NotifyPropertyChanged();
            }
        }

        private LocationLink _SelectedLocationLink;
        public LocationLink SelectedLocationLink
        {
            get { return _SelectedLocationLink; }
            set
            {
                _SelectedLocationLink = value;
                NotifyPropertyChanged();
            }
        }
        #endregion
        
        #region Constructor

        public TacEditorViewModel(IDialogService dialogService)
        {
            this.dialogService = dialogService;
            PlayGameCommand = new RelayCommand(InvokeGamePlayer);
            OpenDatabaseWindowCommand = new RelayCommand(OpenDatabaseWindow, CanOpenDatabaseWindow);
            SaveCommand = new RelayCommand(SaveAsGameProject);
            OpenProjectWindowCommand = new RelayCommand(OpenProjectsWindowDialog);
            AddLocationCommand = new RelayCommand(AddLocation, CanExecuteAddLocation);
            DeleteLocationCommand = new RelayCommand(DeleteLocation, CanExecuteIfSelectedLocationNotNull);
            DeleteLocationLinkCommand = new RelayCommand(DeleteLocationLink, CanExecuteIfSelectedLocationLinkNotNull);
            AddLocationLinkCommand = new RelayCommand(AddLocationLinkWindowDialog, CanExecuteIfSelectedLocationNotNull);
            SaveAsJsonDialogCommand = new RelayCommand(SaveAsJsonDialog);
            LoadJsonDialogCommand = new RelayCommand(LoadJsonDialog);
            InvokeEventPickerDialogCommand = new RelayCommand(InvokeEventPickerWindowDialog);
            //DeleteActionEventFromLocationCommand = new RelayCommand(DeleteActionEventFromLocation, CanDeleteActionEventFromLocation);
            EditActionEventCommand = new RelayCommand(EditActionEvent);

            this.Game = new Game();
            this.Game.Locations = new ObservableCollection<Location>();
            this.Game.Name = "MyGame";
            this.Game.LastEditDate = DateTime.Now;
        }

        public TacEditorViewModel() { }
        #endregion

        #region Commands
        public void InvokeGamePlayer()
        {
            var startActionEvent = (this.Game.StartLocation?
                                .FirstTimeEvents?
                                .FirstOrDefault(x => x is ActionEvent_ShowPage) as ActionEvent_ShowPage);

            if (startActionEvent != null && startActionEvent.Page != null)
            {
                var saveLoadMg = new SaveLoadManager();
                string json = saveLoadMg.GetJson(Game);
                Game newGame = saveLoadMg.LoadGameFromJson(json);

                var gameWindow = new GamePlayer.GamePlayerWindow(newGame);
                gameWindow.Show();
            }
            else
            {
                MessageBox.Show("No starting page found.", "Loading Page Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        public void OpenDatabaseWindow()
        {
            var dbViewModel = new DatabaseViewModel(this.Game);
            dialogService.ShowDialog(dbViewModel);
        }

        public bool CanOpenDatabaseWindow(object placeholder)
        {
            return this.Game != null;
        }

        public void OpenProjectsWindowDialog()
        {
            var saveLoadMG = new SaveLoadManager();
            this.Game = saveLoadMG.LoadGameDialog();
        }

        public void SaveAsGameProject()
        {
            var saveLoadMG = new SaveLoadManager();
            saveLoadMG.SaveAsGameDialog(this.Game);
        }

        public void SaveAsJsonDialog()
        {
            var saveLoadMG = new SaveLoadManager();
            saveLoadMG.SaveGameAsJsonDialog(this.Game);
        }

        public void LoadJsonDialog()
        {
            SaveLoadManager jsonManager = new SaveLoadManager();
            var game = jsonManager.LoadGameAsJsonDialog();

            if (game != null)
                this.Game = game;
        }

        public void AddLocation()
        {
            int locationCount = this.Game.Locations.Count + 1;
            string locationName = $"Location {locationCount}";
            string description = "Enter your description here.";

            var newLocation = new Location(this.Game)
            {
                Name = locationName,
                Description = description,
                LocationLinks = new ObservableCollection<LocationLink>()
            };

            this.Game.Locations.Add(newLocation);
            this.SelectedLocation = newLocation;
        }

        public bool CanExecuteAddLocation(object locationCount)
        {
            if (locationCount == null)
                return false;

            if ((int)locationCount >= 100)
                return false;
            else
                return true;
        }

        public void AddLocationLinkWindowDialog()
        {
            SelectLocationViewModel addLocationLinkViewModel = new SelectLocationViewModel(this.Game.Locations);

            bool? dialog = this.dialogService.ShowDialog(addLocationLinkViewModel);

            if (dialog == true)
            {
                LocationLink linkedLocation = new LocationLink(this.SelectedLocation, addLocationLinkViewModel.SelectedLocation);
                
                this.SelectedLocation.LocationLinks.Add(linkedLocation);
            }
        }

        public void DeleteLocation()
        {
            MessageBoxResult dialogResult = MessageBox.Show("Delete location?", "Question", MessageBoxButton.OKCancel, MessageBoxImage.Question);
            if (dialogResult == MessageBoxResult.OK)
            {
                var locationLinksContaingSelectedLocation = Game.Locations.Select(x => x.LocationLinks.Where(link => link == SelectedLocationLink).Select(link => link));

                foreach(var location in Game.Locations)
                {
                    while (location.LocationLinks.Any(x => x.DestinationLocation == SelectedLocation))
                    {
                        var foundLocation = location.LocationLinks.First(x => x.DestinationLocation == SelectedLocation);
                        location.LocationLinks.Remove(foundLocation);
                    }
                }

                this.Game.Locations.Remove(this.SelectedLocation);
            }
        }

        public void DeleteLocationLink()
        {
            MessageBoxResult dialogResult = MessageBox.Show("Delete locationlink?", "Question", MessageBoxButton.OKCancel, MessageBoxImage.Question);
            if (dialogResult == MessageBoxResult.OK)
            {
                this.SelectedLocation.LocationLinks.Remove(this.SelectedLocationLink);
            }
        }

        public bool CanExecuteIfSelectedLocationLinkNotNull(object placeholder)
        {
            return SelectedLocationLink != null;
        }

        public bool CanExecuteIfSelectedLocationNotNull(object placeholder)
        {
            return SelectedLocation != null;
        }

        public void InvokeEventPickerWindowDialog(object eventPool)
        {
            var eventPickerVM = new EventPickerViewModel();
            bool? dialog = this.dialogService.ShowDialog(eventPickerVM);

            if (dialog == true)
            {
                var events = eventPool as ObservableCollection<IActionEvent>;

                if (events == null)
                    return;

                switch (eventPickerVM.ActionEventType)
                {
                    case EventPickerViewModel.EActionEventType.ShowPage:
                        var txtEditorVM = new TextEditorViewModel();
                        bool? dialogextEditor = this.dialogService.ShowDialog(txtEditorVM);

                        if (dialogextEditor == true)
                        {
                            string inlines = $"{(txtEditorVM.Text != null ? txtEditorVM.Text : "No user input.")}";
                            var newSPEvent = new ActionEvent_ShowPage(inlines);

                            events.Add(newSPEvent);
                            //this.SelectedActionEvent = newSPEvent;
                        }
                        break;
                    case EventPickerViewModel.EActionEventType.Teleport:
                        var locationPickerVM = new SelectLocationViewModel(this.Game.Locations);
                        bool? dialoglocationPickerEditor = this.dialogService.ShowDialog(locationPickerVM);

                        if (dialoglocationPickerEditor == true)
                        {
                            var newTPEvent = new ActionEvent_Teleport(locationPickerVM.SelectedLocation);
                            events.Add(newTPEvent);
                        }
                        break;
                    case EventPickerViewModel.EActionEventType.Condition:
                        var conditionVM = new ConditionEditorViewModel(this.Game);
                        bool? dialogConditionEditor = this.dialogService.ShowDialog(conditionVM);

                        if (dialogConditionEditor == true)
                        {
                            var newCEvent = new ActionEvent_Condition(conditionVM.Condition);
                            events.Add(newCEvent);
                        }
                        break;
                    case EventPickerViewModel.EActionEventType.ChangeVariable:
                        var changeVariableVM = new ChangeVariableViewModel(this.Game.GameVariables);
                        bool? dialogChangeVariable = this.dialogService.ShowDialog(changeVariableVM);

                        if (dialogChangeVariable == true)
                        {
                            var newCVEvent = changeVariableVM.ActionEvent;
                            events.Add(newCVEvent);
                        }
                        break;
                    case EventPickerViewModel.EActionEventType.ChangeLocation:
                        var changeLocationVM = new ChangeLocationViewModel(this.Game.Locations);
                        bool? dialogChangeLocation = this.dialogService.ShowDialog(changeLocationVM);

                        if (dialogChangeLocation == true)
                        {
                            var newCLEvent = changeLocationVM.ActionEvent_ChangeLocation;
                            events.Add(newCLEvent);
                        }
                        break;
                }
            }
        }

        public void EditActionEvent(object selectedActionEvent)
        {
            if (selectedActionEvent is ActionEvent_ShowPage aeShowPage)
            {
                var txtEditorVM = new TextEditorViewModel();
                txtEditorVM.Text = aeShowPage.Page.Text;

                bool? dialogextEditor = this.dialogService.ShowDialog(txtEditorVM);

                if (dialogextEditor == true)
                {
                    aeShowPage.Update(txtEditorVM.Text);
                }
            }
        }
        #endregion
    }
}
