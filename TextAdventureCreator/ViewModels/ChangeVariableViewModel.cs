﻿using SharedComponents.BaseImplementations;
using SharedComponents.Models.MethodModels;
using SharedComponents.Models.MethodModels.Conditions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextAdventureCreator.ViewModels
{
    class ChangeVariableViewModel : BaseNotifyPropertyChanged, IDialogRequestClose
    {
        #region Properties
        public event EventHandler<DialogCloseRequestedEventArgs> CloseRequested;
        public RelayCommand OkCommand { get; private set; }
        public RelayCommand CancelCommand { get; private set; }

        public ObservableCollection<GameVariable> GameVariables { get; set; }

        private ActionEvent_ChangeVariable _ActionEvent;
        public ActionEvent_ChangeVariable ActionEvent
        {
            get { return _ActionEvent; }
            set
            {
                _ActionEvent = value;
                NotifyPropertyChanged();
            }
        }

        private int _ConstantValue;
        public int ConstantValue
        {
            get { return _ConstantValue; }
            set
            {
                _ConstantValue = value;
                NotifyPropertyChanged();
            }
        }

        private bool _VariableIsChecked;
        public bool VariableIsChecked
        {
            get { return _VariableIsChecked; }
            set
            {
                _VariableIsChecked = value;
                NotifyPropertyChanged();
            }
        }
        #endregion

        #region Constructor
        public ChangeVariableViewModel() { }

        public ChangeVariableViewModel(ObservableCollection<GameVariable> selectableGameVariables)
        {
            OkCommand = new RelayCommand(Ok, CanExecuteOk);
            CancelCommand = new RelayCommand(p => CloseRequested?.Invoke(this, new DialogCloseRequestedEventArgs(false)));

            this.ActionEvent = new ActionEvent_ChangeVariable();
            this.VariableIsChecked = true;
            this.GameVariables = selectableGameVariables;
        }
        #endregion

        #region Methodes
        public void Ok()
        {
            var firstVariableName = !String.IsNullOrWhiteSpace(ActionEvent.Variable.Name) ? ActionEvent.Variable.Name : $"(No Name Set) {ActionEvent.Variable.Value}";

            string secondVariableName;

            if (!VariableIsChecked)
            {
                secondVariableName = ConstantValue.ToString();
                this.ActionEvent.Operant = new GameVariable(ConstantValue);
            }
            else
                secondVariableName = !String.IsNullOrWhiteSpace(ActionEvent.Operant.Name) ? ActionEvent.Operant.Name : $"(No Name Set) {ActionEvent.Operant.Value}";

            this.ActionEvent.Name = $"Change Variable: {firstVariableName} {ActionEvent.eVariableOperation} {secondVariableName}";

            CloseRequested?.Invoke(this, new DialogCloseRequestedEventArgs(true));
        }

        public bool CanExecuteOk(object placeholder)
        {
            return ActionEvent.Variable != null && (ActionEvent.Operant != null || !VariableIsChecked);
        }
        #endregion
    }
}
