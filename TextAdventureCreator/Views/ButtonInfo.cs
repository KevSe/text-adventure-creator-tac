﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace TextAdventureCreator.Views
{

    public class ButtonInfo
    {
        public string Name { get; set; }
        public string CommandParameterString { get; set; }
        public Geometry IconData { get; set; }

        public ButtonInfo(string name, string commandParameter, Geometry iconData)
        {
            this.Name = name;
            this.CommandParameterString = commandParameter;
            this.IconData = iconData;
        }

    }
}
