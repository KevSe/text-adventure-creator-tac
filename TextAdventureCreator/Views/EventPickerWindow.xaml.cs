﻿using SharedComponents.BaseImplementations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace TextAdventureCreator.Views
{
    /// <summary>
    /// Interaktionslogik für EventPickerWindow.xaml
    /// </summary>
    public partial class EventPickerWindow : Window, IDialog
    {
        public EventPickerWindow()
        {
            InitializeComponent();

            var resourceDic = new ResourceDictionary();
            resourceDic.Source =
                new Uri(";component/ResourceDictionaries/Icons.xaml",
                        UriKind.RelativeOrAbsolute);

            lbButtons.Items.Add(
                new ButtonInfo("Show\nPage",  "showpage", (resourceDic["Icon.Page"] as Geometry)));
            lbButtons.Items.Add(
                new ButtonInfo("Change Variable", "variable", (resourceDic["Icon.Variable"] as Geometry)));
            lbButtons.Items.Add(
                new ButtonInfo("Change Location", "location", (resourceDic["Icon.Location"] as Geometry)));
            lbButtons.Items.Add(
                new ButtonInfo("Teleport to Location", "teleport", (resourceDic["Icon.ManMoving"] as Geometry)));
            lbButtons.Items.Add(
                new ButtonInfo("Conditional Branch", "condition", (resourceDic["Icon.Condition"] as Geometry)));
        }
    }
}
