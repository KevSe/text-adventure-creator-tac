﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace TextAdventureCreator.Models
{
    public class ActionEventMenuItem
    {
        public Type ActionEventType { get; set; }
        public Geometry IconSvg { get; set; }
        public string Name { get; set; }
    }
}
