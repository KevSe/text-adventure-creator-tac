﻿using SharedComponents.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TextAdventureCreator.UserControls.DatabaseViews
{
    /// <summary>
    /// Interaktionslogik für GameItemsView.xaml
    /// </summary>
    public partial class GameItemsView : UserControl
    {
        public ObservableCollection<GameItem> Items
        {
            get { return (ObservableCollection<GameItem>)GetValue(ItemsProperty); }
            set { SetValue(ItemsProperty, value); }
        }

        public static readonly DependencyProperty ItemsProperty =
            DependencyProperty.Register("Items", typeof(ObservableCollection<GameItem>), typeof(GameItemsView), new PropertyMetadata(null));


        public GameItem SelectedItem
        {
            get { return (GameItem)GetValue(SelectedItemProperty); }
            set { SetValue(SelectedItemProperty, value); }
        }

        public static readonly DependencyProperty SelectedItemProperty =
            DependencyProperty.Register("SelectedItem", typeof(GameItem), typeof(GameItemsView), new PropertyMetadata(null));




        public GameItemsView()
        {
            InitializeComponent();
        }
    }
}
