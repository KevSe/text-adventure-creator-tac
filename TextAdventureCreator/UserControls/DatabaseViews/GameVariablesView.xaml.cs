﻿using SharedComponents.Models.MethodModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TextAdventureCreator.UserControls.DatabaseViews
{
    /// <summary>
    /// Interaktionslogik für GameVariablesView.xaml
    /// </summary>
    public partial class GameVariablesView : UserControl
    {
        public ObservableCollection<GameVariable> GameVariables
        {
            get { return (ObservableCollection<GameVariable>)GetValue(GameVariablesProperty); }
            set { SetValue(GameVariablesProperty, value); }
        }
        public static readonly DependencyProperty GameVariablesProperty =
            DependencyProperty.Register("GameVariables", typeof(ObservableCollection<GameVariable>), typeof(GameVariablesView), new PropertyMetadata(null));


        public GameVariable SelectedGameVariable
        {
            get { return (GameVariable)GetValue(SelectedGameVariableProperty); }
            set { SetValue(SelectedGameVariableProperty, value); }
        }
        public static readonly DependencyProperty SelectedGameVariableProperty =
            DependencyProperty.Register("SelectedGameVariable", typeof(GameVariable), typeof(GameVariablesView), new PropertyMetadata(null));



        public GameVariablesView()
        {
            InitializeComponent();
        }
    }
}
