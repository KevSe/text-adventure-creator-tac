﻿using SharedComponents.BaseImplementations;
using SharedComponents.Interfaces;
using SharedComponents.Models.MethodModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TextAdventureCreator.ViewModels;
using TextAdventureCreator.Views;

namespace TextAdventureCreator.UserControls
{
    /// <summary>
    /// Interaktionslogik für EventListControl.xaml
    /// </summary>
    public partial class EventListControl : UserControl
    {
        public ObservableCollection<IActionEvent> ActionEvents
        {
            get { return (ObservableCollection<IActionEvent>)GetValue(ActionEventsProperty); }
            set { SetValue(ActionEventsProperty, value); }
        }
        public static readonly DependencyProperty ActionEventsProperty =
            DependencyProperty.Register("ActionEvents", typeof(ObservableCollection<IActionEvent>), typeof(EventListControl), new PropertyMetadata(null));

        public IActionEvent SelectedActionEvent
        {
            get { return (IActionEvent)GetValue(SelectedActionEventProperty); }
            set { SetValue(SelectedActionEventProperty, value); }
        }
        public static readonly DependencyProperty SelectedActionEventProperty =
            DependencyProperty.Register("SelectedActionEvent", typeof(IActionEvent), typeof(EventListControl), new PropertyMetadata(null));

        public RelayCommand DeleteEventCommand
        {
            get { return (RelayCommand)GetValue(DeleteEventCommandProperty); }
            set { SetValue(DeleteEventCommandProperty, value); }
        }

        // Using a DependencyProperty as the backing store for DeleteEventCommand.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty DeleteEventCommandProperty =
            DependencyProperty.Register("DeleteEventCommand", typeof(RelayCommand), typeof(EventListControl), new PropertyMetadata(null));

        public RelayCommand MoveEventCommand
        {
            get { return (RelayCommand)GetValue(MoveEventCommandProperty); }
            set { SetValue(MoveEventCommandProperty, value); }
        }

        // Using a DependencyProperty as the backing store for MoveEventCommand.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty MoveEventCommandProperty =
            DependencyProperty.Register("MoveEventCommand", typeof(RelayCommand), typeof(EventListControl), new PropertyMetadata(null));


        public EventListControl()
        {
            InitializeComponent();
            this.DeleteEventCommand = new RelayCommand(DeleteEvent, CanDeleteActionEventFromLocation);
            this.MoveEventCommand = new RelayCommand(MoveEvent, CanMoveEvent);
        }

        public void DeleteEvent(object eventPool)
        {
            MessageBoxResult dialogResult = MessageBox.Show("Delete event?", "Question", MessageBoxButton.OKCancel, MessageBoxImage.Question);
            if (dialogResult == MessageBoxResult.OK)
                ActionEvents.Remove(this.SelectedActionEvent);
        }

        public bool CanDeleteActionEventFromLocation(object placeholder)
        {
            return this.SelectedActionEvent != null;
        }

        public void MoveEvent(object direction)
        {
            string dir = (string)direction;
            int index = ActionEvents.IndexOf(SelectedActionEvent);
            var aevent = SelectedActionEvent;

            ActionEvents.Remove(aevent);

            switch(dir)
            {
                case "Up":
                    ActionEvents.Insert(index - 1, aevent);
                    break;
                case "Down":
                    ActionEvents.Insert(index + 1, aevent);
                    break;
            }

            SelectedActionEvent = aevent;
        }

        public bool CanMoveEvent(object direction)
        {
            if (SelectedActionEvent == null)
                return false;

            string dir = (string)direction;
            int index = ActionEvents.IndexOf(SelectedActionEvent);

            if (dir == "Up" && index > 0)
                return true;

            if (dir == "Down" && index < ActionEvents.Count - 1)
                return true;

            return false;
        }
    }
}
