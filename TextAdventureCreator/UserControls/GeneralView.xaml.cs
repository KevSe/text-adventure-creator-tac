﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using SharedComponents.Models;

namespace TextAdventureCreator.UserControls
{
    /// <summary>
    /// Interaktionslogik für GeneralView.xaml
    /// </summary>
    public partial class GeneralView : UserControl
    {
        public Location SelectedLocation
        {
            get { return (Location)GetValue(SelectedLocationProperty); }
            set { SetValue(SelectedLocationProperty, value); }
        }

        // Using a DependencyProperty as the backing store for SelectedLocation.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SelectedLocationProperty =
            DependencyProperty.Register("SelectedLocation", typeof(Location), typeof(GeneralView), new PropertyMetadata(null));


        public GeneralView()
        {
            InitializeComponent();
        }
    }
}
