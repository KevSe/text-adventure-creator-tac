﻿using SharedComponents.BaseImplementations;
using SharedComponents.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TextAdventureCreator.UserControls
{
    /// <summary>
    /// Interaktionslogik für LocationLinkView.xaml
    /// </summary>
    public partial class LocationLinkView : UserControl
    {
        public ObservableCollection<LocationLink> LocationLinks
        {
            get { return (ObservableCollection<LocationLink>)GetValue(LocationLinksProperty); }
            set { SetValue(LocationLinksProperty, value); }
        }

        // Using a DependencyProperty as the backing store for LinkedLocations.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty LocationLinksProperty =
            DependencyProperty.Register("LocationLinks", typeof(ObservableCollection<LocationLink>), typeof(LocationLinkView), new PropertyMetadata(null));


        public LocationLink SelectedLocationLink
        {
            get { return (LocationLink)GetValue(SelectedLocationLinkProperty); }
            set { SetValue(SelectedLocationLinkProperty, value); }
        }

        // Using a DependencyProperty as the backing store for SelectedLocationLink.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SelectedLocationLinkProperty =
            DependencyProperty.Register("SelectedLocationLink", typeof(LocationLink), typeof(LocationLinkView), new PropertyMetadata(null));


        public RelayCommand AddCommand
        {
            get { return (RelayCommand)GetValue(AddCommandProperty); }
            set { SetValue(AddCommandProperty, value); }
        }

        // Using a DependencyProperty as the backing store for AddCommand.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty AddCommandProperty =
            DependencyProperty.Register("AddCommand", typeof(RelayCommand), typeof(LocationLinkView), new PropertyMetadata(null));

        
        public RelayCommand DeleteCommand
        {
            get { return (RelayCommand)GetValue(DeleteCommandProperty); }
            set { SetValue(DeleteCommandProperty, value); }
        }

        // Using a DependencyProperty as the backing store for DeleteCommand.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty DeleteCommandProperty =
            DependencyProperty.Register("DeleteCommand", typeof(RelayCommand), typeof(LocationLinkView), new PropertyMetadata(null));


        
        public LocationLinkView()
        {
            InitializeComponent();
        }
    }
}
