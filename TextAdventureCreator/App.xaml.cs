﻿using SharedComponents.BaseImplementations;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using TextAdventureCreator.ViewModels;

namespace TextAdventureCreator
{
    /// <summary>
    /// Interaktionslogik für "App.xaml"
    /// </summary>
    public partial class App : Application
    {
        public void StartApplication(object sender, StartupEventArgs e)
        {
            IDialogService dialogService = new DialogService(MainWindow);
            dialogService.Register<SelectLocationViewModel, Views.SelectLocationWindow>();
            dialogService.Register<TextEditorViewModel, Views.TextEditorWindow>();
            dialogService.Register<EventPickerViewModel, Views.EventPickerWindow>();
            dialogService.Register<DatabaseViewModel, Views.DatabaseWindow>();
            dialogService.Register<ConditionEditorViewModel, Views.ConditionEditorWindow>();
            dialogService.Register<ChangeVariableViewModel, Views.ChangeVariableWindow>();
            dialogService.Register<ChangeLocationViewModel, Views.ChangeLocationWindow>();

            var view = new Views.TacEditorWindow();
            TacEditorViewModel mainViewModel = new TacEditorViewModel(dialogService);
            view.DataContext = mainViewModel;
            view.Show();


        }
    }
}
